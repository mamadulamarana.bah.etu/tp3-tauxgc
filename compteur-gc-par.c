#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>

#define SEUIL 10

struct params {
    char *tab;
    unsigned long length;
    int start;
    int end;
    int ret;
};
unsigned long compteur_gc_par(char *bloc, unsigned long taille, int t);

void *compteur_gc_par_wrapper (void *args) {
    struct params *p = (struct params *) args;
    int i;
    for (i = p->start; i < p->end; i++) {
        if (p->tab[i] == 'G' || p->tab[i] == 'C'){
            p->ret += 1;
        }
    }
    return NULL;
}

unsigned long compteur_gc_par(char *bloc, unsigned long taille, int t) {

    unsigned long cptr = 0;

    pthread_t *th = malloc(sizeof(pthread_t)*t);
    struct params *p = malloc(sizeof(struct params)*t);
    for (int i=0; i<t; i++) {
        p[i].tab = bloc;
        p[i].start = (i* (taille/t) );
        p[i].end = ( ((i+1) == t)? taille : (i+1) *(taille/t));
        p[i].length = (taille / t);
        p[i].ret = 0;
        int status = pthread_create(&th[i], NULL, compteur_gc_par_wrapper, (void *)&p[i]);
        if (status != 0) {
            printf("erreur creation thread");
            return -1;
        }
    }

    for(int i=0; i<t; i++) {
        int status = pthread_join(th[i], 0);
        cptr += p[i].ret; 
        if (status != 0) {
            printf("erreur join thread");
            return -1;
        }
    }

    free(p);
    free(th);
    return cptr;
}

int main(int argc, char *argv[]) {
    struct stat st; 
    int fd;
    char *tampon;
    int lus;
    unsigned long cptr = 0;
    off_t taille = 0;
    struct timespec debut, fin;

    assert(argc > 1);

    /* Quelle taille ? */
    assert(stat(argv[1], &st) != -1);
    tampon = malloc(st.st_size);
    assert(tampon != NULL);

    /* Chargement en mémoire */
    fd = open(argv[1], O_RDONLY); 
    assert(fd != -1);
    while ((lus = read(fd, tampon + taille, st.st_size - taille)) > 0)
        taille += lus;
    assert(lus != -1);
    assert(taille == st.st_size);
    close(fd);

    /* Calcul proprement dit */
    assert(clock_gettime(CLOCK_MONOTONIC, &debut) != -1);
    cptr = compteur_gc_par(tampon, taille, atoi(argv[2]));
    assert(clock_gettime(CLOCK_MONOTONIC, &fin) != -1);

    /* Affichage des résultats */
    printf("Nombres de GC:   %ld\n", cptr);
    printf("Taux de GC:      %lf\n", ((double) cptr) / ((double) taille));

    fin.tv_sec  -= debut.tv_sec;
    fin.tv_nsec -= debut.tv_nsec;
    if (fin.tv_nsec < 0) {
        fin.tv_sec--;
        fin.tv_nsec += 1000000000;
    }
    printf("Durée de calcul: %ld.%09ld\n", fin.tv_sec, fin.tv_nsec);
    printf("(Attention: très peu de chiffres après la virgule sont réellement significatifs !)\n");

    free(tampon);
    
    return 0;
}


/**
    if (taille <= SEUIL) {
        unsigned long i, cptr = 0;
        for (i = 0; i < taille; i++)
            if (bloc[i] == 'G' || bloc[i] == 'C')
                cptr++;
        return cptr;
    }

*/